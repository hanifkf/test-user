package main

import "net/http"

func main() {
	http.HandleFunc("/ping", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("oke"))
	})

	http.ListenAndServe(":9000", nil)
}
